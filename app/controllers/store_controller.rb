class StoreController < ApplicationController
  def index
    @list = Store.all
  end

  def number
    save_to_db(params[:rndnumber].to_i)
    render json: { result: "Ok", num: params[:rndnumber].to_i }
  rescue => e
    render json: { result: "Error", err: e.message }
  end

  def latest
    render json: Store.last
  end

  private

  def save_to_db(num = 0)
    Store.create(rnd: num)
  end
end
