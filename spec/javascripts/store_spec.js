//= require jquery
describe("jquery library", function() {
  it("will check jquery library", function() {
    expect(jQuery).toBeDefined();
  });
});

describe("store number 100", function() {
  var rnd_number = Math.floor((Math.random() * 1000) + 1);

  it("will save to database", function() {
    var number;

    $.ajax({
      type: "POST",
      url: "/store/number",
      data: { rndnumber: rnd_number },
      async: false,
      success: function(response) {
        number = response;
      }
    });

    expect(number.result).toBe("Ok");
  });

  it("will get latest number", function() {
    var number;

    $.ajax({
      type: "GET",
      url: "/store/number/latest",
      async: false,
      success: function(response) {
        number = response;
      }
    });

    expect(number.rnd).toBe(rnd_number);
  });
});
