Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :store

  post "/store/number" => "store#number"
  get "/store/number/latest" => "store#latest"

  root "store#index"
end
